def func(x):
# Функция для проверки положительного числа
    if x < 0:
    # Обработка ситуации, если пользователь ввел отрицательное значение        
        return "Sorry, x < 0, repeat input"
    if x%3 == 0 and x%5 == 0:
        return "foo bar"
    elif x%3 == 0:
        return "foo"
    elif x%5 == 0:
        return "bar"
    else:
        return x

if __name__ == '__main__':
    assert func(15) == "foo bar"
    assert func(25) == "bar"
    assert func(24) == "foo"
    assert func(17) == 17
    assert func(-1) == "Sorry, x < 0, repeat input"
